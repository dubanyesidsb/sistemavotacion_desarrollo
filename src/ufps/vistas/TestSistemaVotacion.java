/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import Negocio.SistemaVotacion;

/**
 *
 * @author estudiante
 */
public class TestSistemaVotacion {
    public static void main(String[] args) {
        String urlDpto="https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/departamento.csv";
        String urlMun="https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/municipios.csv";
        String urlPer="https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/personas.csv";
        SistemaVotacion s=new SistemaVotacion(urlDpto,urlMun,urlPer);
        System.out.println("Departamentos:"+s.getListadoDpto());
        System.out.println("Personas:"+s.getListadoPersonas());
    }
}
